#ifndef SHIP_H
#define SHIP_H

#include"position.h"

class Ship
{

public:
    Ship(Position start_pos = Position(), Position end_pos = Position(), unsigned lenght = 0): m_start_pos(start_pos),
                                                                                               m_end_pos(end_pos),
                                                                                               m_lenght(lenght),
                                                                                               m_current_lenght(lenght)
{
}

    unsigned getCurrentLenght() const
    {
        return m_current_lenght;
    }

    bool decCurrentLenght();            //decrement current lenght

    Position getStartPos() const
    {
        return m_start_pos;
    }

    Position getEndPos() const
    {
        return m_end_pos;
    }

    void setStartPos(Position start_pos)
    {
        m_start_pos = start_pos;
    }

    void setEndPos(Position end_pos)
    {
        m_end_pos = end_pos;
    }


protected:
    Position m_start_pos;
    Position m_end_pos;

    unsigned m_lenght;
    unsigned m_current_lenght;
};

#endif // SHIP_H
