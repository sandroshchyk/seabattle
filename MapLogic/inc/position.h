#ifndef POSITION_H
#define POSITION_H

#include<climits>

class Position
{
public:
    Position(int x = INT_MAX, int y = INT_MAX): m_x(x), m_y(y)
    {
    }

    int getX()
    {
        return m_x;
    }

    int getY()
    {
        return m_y;
    }

    void setX(int x)
    {
        m_x = x;
    }

    void setY(int y)
    {
        m_y = y;
    }

    bool operator==(Position& pos)
    {
        bool result = false;
        if(m_x == pos.getX() && m_y == pos.getY())
        {
           result = true;
        }

        return result;
    }


protected:
    int m_x;
    int m_y;
};

#endif // POSITION_H
