#ifndef SHIPS_MAP_H
#define SHIPS_MAP_H


#include"ship.h"
#include"navy_descriptor.h"
#include"position.h"

enum state_on_map {free_cell, hit_free_cell, full_hp_ship, summed_ship, destroyed_ship};

class ShipsMap
{


public:

    ShipsMap(unsigned map_size, NavyDescriptor navy = NavyDescriptor());

    state_on_map**  getMap() const   //XXX change to const state_on_map**
    {
        return m_map;
    }
    state_on_map getStateInPosition(Position pos)
    {
        return m_map[pos.getY()][pos.getX()];
    }

    unsigned getNumShips() const
    {
        return m_total_ships;
    }

    unsigned getCurrentNumShips() const
    {
        return m_current_ships;
    }

    unsigned getMapSize() const
    {
        return m_map_size;
    }

    const Ship* getShipsList() const
    {
        return m_ships_list;
    }

    bool mapReadyToUse()
    {
        bool result = false;

        if(m_total_ships == m_current_ships)
            result = true;

        return result;
    }

    bool notDiagonal(Position start, Position end)
    {
        bool result = false;

        if((start.getX() == end.getX()) || (start.getY() == end.getY()))
        {
            result = true;
        }

        return result;
    }

    unsigned getShipSizeFromPosition(Position start, Position end)// parameters must be in correct order
    {
        unsigned ship_size = 0;

        if(start.getX() == end.getX())//vertical direction
        {
            ship_size = (end.getY()+1) - start.getY();
        }
        else
        {
            ship_size = (end.getX()+1) - start.getX();
        }

        return ship_size;
    }

    NavyDescriptor* getNavyDescriptor()
    {
        return &m_navy;
    }

    bool placeShip(Position start, Position end);

    void setShip(NavyDescriptor* navy, unsigned ship_type, Position start_pos, Position end_pos);

    void setCoordsToCorrectOrder(Position& start, Position& end); //left to right and top to bot

    //Placement functions
    bool positionIsEquel(Position input_pos, Position start, Position end);

    bool checkNinePoints(Position ship_start_pos, Position start, Position end);

    bool checkSixTopPoints(Position ship_start_pos, Position start, Position end);

    bool checkSixBottomPoints(Position ship_end_pos, Position start, Position end);

    bool checkThreePoints(Position ship_mid_pos, Position start, Position end);

    bool checkPoints(Position cur_pos,Position start_ship_pos, Position end_ship_pos, Position start, Position end);
    //

    virtual ~ShipsMap();

protected:

    void clearMap();

    unsigned m_map_size;      //size one side
    state_on_map** m_map;

    NavyDescriptor m_navy;
    unsigned m_total_ships;   //show how many ships are totaly

    Ship* m_ships_list;
    unsigned m_current_ships; //show how many ships are on map at time

};

#endif // SHIPS_MAP_H
