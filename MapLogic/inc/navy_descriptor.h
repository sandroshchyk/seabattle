#ifndef NAVY_DESCRIPTOR_H
#define NAVY_DESCRIPTOR_H

#include"ship_info.h"

class NavyDescriptor
{

public:
  NavyDescriptor(unsigned ships_types = 4);
  NavyDescriptor(const NavyDescriptor& Descriptor);
  bool addShipInfo(unsigned ship_type, unsigned ship_size, unsigned quantity);
  void createStandartDescriptor(void);

  virtual ~NavyDescriptor();

  ShipInfo* getShipInfo()
  {
      return m_ships_info;
  }

  unsigned getShipsTypes()
  {
      return m_ships_types;
  }

  bool existThisShip(unsigned size, unsigned& ship_type);

protected:
  unsigned m_ships_types;
  ShipInfo* m_ships_info;


};

#endif // NAVY_DESCRIPTOR_H
