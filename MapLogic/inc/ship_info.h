#ifndef SHIP_INFO_H
#define SHIP_INFO_H

class ShipInfo //this class describe mains ship properties
{
    unsigned m_ship_size;
    unsigned m_quantity;

public:
    void setShipSize(unsigned ship_size)
    {
        m_ship_size = ship_size;
    }

    void setShipQuantity(unsigned quantity)
    {
        m_quantity = quantity;
    }

    unsigned getShipSize() const
    {
        return m_ship_size;
    }

    unsigned getShipQuantity() const
    {
        return m_quantity;
    }

    bool decShipQuantity(); //decrement quantity
};

#endif // SHIP_INFO_H
