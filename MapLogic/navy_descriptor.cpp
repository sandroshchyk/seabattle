#include"navy_descriptor.h"

NavyDescriptor::NavyDescriptor(unsigned ships_types):m_ships_types(ships_types)
{
    m_ships_info = new ShipInfo[m_ships_types];    
    createStandartDescriptor();
}

NavyDescriptor::NavyDescriptor(const NavyDescriptor& Descriptor)
{
    if(this != &Descriptor)
    {
        this->m_ships_types = Descriptor.m_ships_types;
        this->m_ships_info = new ShipInfo[m_ships_types];

        for(unsigned index = 0; index < m_ships_types; index++)
        {
            m_ships_info[index] = Descriptor.m_ships_info[index];
        }
    }
}

NavyDescriptor::~NavyDescriptor()
{
    delete[] m_ships_info;
}

bool NavyDescriptor::addShipInfo(unsigned ship_type, unsigned ship_size, unsigned quantity)
{
    bool result = false;

    if(ship_type < m_ships_types)
    {
        m_ships_info[ship_type].setShipSize(ship_size);
        m_ships_info[ship_type].setShipQuantity(quantity);
        result = true;
    }

    return result;
}

void NavyDescriptor::createStandartDescriptor(void)
{
    unsigned i = 0u,j = m_ships_types;

    for( ; i < m_ships_types; i++, j--)
    {
      addShipInfo(i, i+1u, j);
    }
}

bool NavyDescriptor::existThisShip(unsigned size, unsigned& ship_type)
{
    bool result = false;

    for(unsigned index = 0; index < m_ships_types; index++)
    {
        if( (m_ships_info[index].getShipSize() == size) && (m_ships_info[index].getShipQuantity()!= 0))
        {
            result  = true;
            ship_type = index;
        }
    }

    return result;
}
