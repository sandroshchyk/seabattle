#include"ship_info.h"

bool ShipInfo::decShipQuantity()
{
    bool result = false;

    if(m_quantity > 0)
    {
        --m_quantity;
        result = true;
    }

    return result;
}
