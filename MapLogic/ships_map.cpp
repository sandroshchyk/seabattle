#include"ships_map.h"

ShipsMap::ShipsMap(unsigned map_size, NavyDescriptor navy): m_map_size(map_size),m_navy(navy), m_current_ships(0)
{
    m_map = new state_on_map*[m_map_size];

    for(unsigned arr = 0; arr < m_map_size; arr++) // arr - array
    {
        m_map[arr] = new state_on_map[m_map_size];
    }

    unsigned ships_cnt = 0;
    unsigned ships_types = m_navy.getShipsTypes();
    const ShipInfo* ships_info =  m_navy.getShipInfo();

    for(unsigned index = 0; index < ships_types; index++)
    {
       ships_cnt = ships_info[index].getShipQuantity() + ships_cnt;
    }

    m_total_ships = ships_cnt;
    m_ships_list = new Ship[m_total_ships];

    clearMap();
}

ShipsMap::~ShipsMap()
{
    for(unsigned arr = 0; arr < m_map_size; arr++)
    {
        delete [] m_map[arr];
    }

    delete []m_map;
    delete []m_ships_list;
}

void ShipsMap::clearMap()
{
    for(unsigned row = 0; row < m_map_size; row++)
    {
        for(unsigned column = 0; column < m_map_size; column++)
        {
            m_map[row][column] = free_cell;
        }
    }
}

void ShipsMap::setShip(NavyDescriptor* navy, unsigned ship_type, Position start_pos, Position end_pos)
{
    if(m_current_ships < m_total_ships)
    {
        m_ships_list[m_current_ships].setStartPos(start_pos);
        m_ships_list[m_current_ships].setEndPos(end_pos);

        if(start_pos.getX() == end_pos.getX())//vertical direction
        {
            int x = start_pos.getX();
            int end_pos_y = end_pos.getY();

            for( int y = start_pos.getY(); y <= end_pos_y; y++)
            {
                m_map[x][y] = full_hp_ship;
            }
        }
        else
        {
            int y =start_pos.getY();
            int end_pos_x = end_pos.getX();

            for( int x = start_pos.getX(); x <= end_pos_x; x++)
            {
                    m_map[x][y] = full_hp_ship;
            }
        }
        m_current_ships++;
        navy->getShipInfo()[ship_type].decShipQuantity();
    }
}

void ShipsMap::setCoordsToCorrectOrder(Position& start, Position& end)
{
    int x = 0;
    int y = 0;

    if(start.getY() == end.getY())
    {
        if(start.getX() > end.getX())
        {
            x = start.getX();
            start.setX(end.getX());
            end.setX(x);
        }
    }
    else
    {
        if(start.getY() > end.getY())
        {
            y = start.getY();
            start.setY(end.getY());
            end.setY(y);
        }
    }
}

bool ShipsMap::checkNinePoints(Position ship_start_pos, Position start, Position end)
{
    bool result = false;
    Position cur_pos = ship_start_pos;

    result |= positionIsEquel(cur_pos,start, end);

    cur_pos.setX(cur_pos.getX()-1);
    result |= positionIsEquel(cur_pos,start, end);

    cur_pos.setX(cur_pos.getX()+2);
    result |= positionIsEquel(cur_pos,start, end);

    cur_pos.setY(cur_pos.getY()-1);
    result |= positionIsEquel(cur_pos,start, end);

    cur_pos.setX(cur_pos.getX()-1);
    result |= positionIsEquel(cur_pos,start, end);

    cur_pos.setX(cur_pos.getX()-1);
    result |= positionIsEquel(cur_pos,start, end);

    cur_pos.setY(cur_pos.getY()+2);
    result |= positionIsEquel(cur_pos,start, end);

    cur_pos.setX(cur_pos.getX()+1);
    result |= positionIsEquel(cur_pos,start, end);

    cur_pos.setX(cur_pos.getX()+1);
    result |= positionIsEquel(cur_pos,start, end);

    return result;
}


bool ShipsMap::checkSixTopPoints(Position ship_start_pos, Position start, Position end)
{
    bool result = false;
    Position cur_pos = ship_start_pos;

    result |= positionIsEquel(cur_pos,start, end);

    cur_pos.setX(cur_pos.getX()-1);
    result |= positionIsEquel(cur_pos,start, end);

    cur_pos.setX(cur_pos.getX()+2);
    result |= positionIsEquel(cur_pos,start, end);

    cur_pos.setY(cur_pos.getY()-1);
    result |= positionIsEquel(cur_pos,start, end);

    cur_pos.setX(cur_pos.getX()-1);
    result |= positionIsEquel(cur_pos,start, end);

    cur_pos.setX(cur_pos.getX()-1);
    result |= positionIsEquel(cur_pos,start, end);

    return result;
}

bool ShipsMap::checkSixBottomPoints(Position ship_end_pos, Position start, Position end)
{
    bool result = false;
    Position cur_pos = ship_end_pos;

    result |= positionIsEquel(cur_pos,start, end);

    cur_pos.setX(cur_pos.getX()-1);
    result |= positionIsEquel(cur_pos,start, end);

    cur_pos.setX(cur_pos.getX()+2);
    result |= positionIsEquel(cur_pos,start, end);

    cur_pos.setY(cur_pos.getY()+1);
    result |= positionIsEquel(cur_pos,start, end);

    cur_pos.setX(cur_pos.getX()-1);
    result |= positionIsEquel(cur_pos,start, end);

    cur_pos.setX(cur_pos.getX()-1);
    result |= positionIsEquel(cur_pos,start, end);

    return result;
}


bool ShipsMap::checkThreePoints(Position ship_mid_pos, Position start, Position end)
{
    bool result = false;
    Position cur_pos = ship_mid_pos;

    result |= positionIsEquel(cur_pos,start, end);

    cur_pos.setX(cur_pos.getX()-1);
    result |= positionIsEquel(cur_pos,start, end);

    cur_pos.setX(cur_pos.getX()+2);
    result |= positionIsEquel(cur_pos,start, end);

    return result;
}

bool ShipsMap::checkPoints(Position cur_pos,Position start_ship_pos, Position end_ship_pos, Position start, Position end)
{
    Position start_pos = start_ship_pos;
    Position end_pos = end_ship_pos;

    bool result = false;

    if( cur_pos == start_pos || cur_pos == end_pos)
    {
        if(cur_pos == start_pos)
        {
            result |= checkSixTopPoints(cur_pos, start, end);
        }
        else
        {
            result |= checkSixBottomPoints(cur_pos, start, end);
        }
    }
    else
    {
        result |= checkThreePoints(cur_pos, start, end);
    }

    return result;
}




bool ShipsMap::placeShip(Position start, Position end)
{

    bool result = false;

    Position cur_pos;
    Position start_pos;
    Position end_pos;

    for(unsigned index = 0; index < m_current_ships; index++)
    {
        start_pos = m_ships_list[index].getStartPos();
        end_pos = m_ships_list[index].getEndPos();
        cur_pos = start_pos;
        unsigned ship_size = getShipSizeFromPosition(start_pos, end_pos);

        if(ship_size == 1)
        {
            result = checkNinePoints(start_pos, start, end);
        }
        else
        {
            //ship direction
            if(start_pos.getX() == end_pos.getX())
            {
                for(unsigned position = 0; position < ship_size; position++, cur_pos.setY(cur_pos.getY()+1))
                {
                    result |= checkPoints(cur_pos, start_pos, end_pos, start, end);
                }
            }
            else
            {
                for(unsigned position = 0; position < ship_size; position++, cur_pos.setX(cur_pos.getX()+1))
                {
                    result |= checkPoints(cur_pos, start_pos, end_pos, start, end);
                }
            }
        }
    }

    return result;
}


bool ShipsMap::positionIsEquel(Position input_pos, Position start, Position end)
{
    bool result = false;
    unsigned proection_size = getShipSizeFromPosition(start, end);
    Position cur_pos = start;

    if(start.getX() == end.getX())
    {
        for(unsigned pos = 0; pos < proection_size; pos++, cur_pos.setY(cur_pos.getY()+1))
        {
            if(input_pos == cur_pos)
                result = true;
        }
    }
    else
    {
        for(unsigned pos = 0; pos < proection_size; pos++, cur_pos.setX(cur_pos.getX()+1))
        {
            if(input_pos == cur_pos)
                result = true;
        }

    }
    return result;
}

