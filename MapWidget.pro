INCLUDE_DIR = .
EXPORTS_DIR = $${INCLUDE_DIR}/MapLogic/inc \
              $${INCLUDE_DIR}/MapGraphic/inc \
              $${INCLUDE_DIR}/UI/inc

INCLUDEPATH = $${INCLUDE_DIR} \
    $${EXPORTS_DIR}

SOURCES += \
    main.cpp \
    map_widget.cpp \
    MapLogic/navy_descriptor.cpp \
    MapLogic/ships_map.cpp \
    MapLogic/ship_info.cpp \
    MapLogic/ship.cpp \
    MapGraphic/map_graphic.cpp \
    UI/main_window.cpp

HEADERS += \
    map_widget.h \
    ships_map.h \
    ship_info.h \
    ship.h \
    position.h \
    navy_descriptor.h \
    MapGraphic/inc/map_graphic.h \
    UI/MainWindow.h \
    UI/inc/main_window.h
