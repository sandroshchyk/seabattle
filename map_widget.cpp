#include"map_widget.h"

MapWidget::MapWidget(bool map_type, unsigned map_size, QWidget* parent): QWidget(parent),
                                                                         m_map(map_size),
                                                                         m_map_graphic(&m_map,map_type, this)
{
    setAttribute(Qt::WA_StaticContents);
    setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);
    connect(&m_map_graphic, SIGNAL(sendCoords(QPoint, QPoint)),SLOT(recvCoords(QPoint, QPoint)));
    connect(&m_map_graphic, SIGNAL(sendPosition(int,int)),SLOT(recvPosition(int,int)));
}

void MapWidget::recvCoords(QPoint start_pos, QPoint end_pos)
{
    Position start, end;

    start.setX(start_pos.x());
    start.setY(start_pos.y());

    end.setX(end_pos.x());
    end.setY(end_pos.y());

    if(m_map.notDiagonal(start,end))
    {
        m_map.setCoordsToCorrectOrder(start, end);
        NavyDescriptor* navy_info = NULL;
        unsigned ship_type = ~0u;

        navy_info = m_map.getNavyDescriptor();
        unsigned ship_size = m_map.getShipSizeFromPosition(start, end);

        if(navy_info->existThisShip(ship_size, ship_type))
        {
            if(!m_map.placeShip(start, end))
            {
                m_map.setShip(navy_info,ship_type, start, end);
                m_map_graphic.update();
            }
            else
            {
                QMessageBox::information(this, tr("Attention"), tr("Please read the placement rules."),QMessageBox::Ok);
            }
        }
        else
        {
            if(m_map.mapReadyToUse())
            {
                m_map_graphic.setPlasementFlag(true);
            }
            else
            {
                QMessageBox::information(this, tr("Attention"), tr("Inncorrect ship size."),QMessageBox::Ok);
            }
        }
    }
    else
    {
        QMessageBox::information(this, tr("Attention"), tr("Incorrect coords. Coords must be not diagonal."),QMessageBox::Ok);
    }
}

void MapWidget::recvPosition(int x, int y)
{
    if(m_game_type)
    {

    }
    else
    {

    }
}

QSize MapWidget::sizeHint() const
{
    return this->m_map_graphic.sizeHint();
}
