#include"map_graphic.h"

MapGraphic::MapGraphic(ShipsMap* map, bool map_type, QWidget* parent, int cell_size): QWidget(parent),m_enable_enemy_map(false)
{
    m_placement_over = false;
    m_click_cnt = 0;
    m_map = map;
    m_mapSize = m_map->getMapSize();
    m_curColor = Qt::red;
    m_cellSize = cell_size;

    m_map_type = map_type;

    m_rect.setHeight(m_mapSize * m_cellSize);
    m_rect.setWidth(m_mapSize * m_cellSize);
}

QSize MapGraphic::sizeHint() const
{
    QSize size;
    size.setHeight(m_cellSize * m_mapSize);
    size.setWidth(m_cellSize * m_mapSize);
    size += QSize(1,1);
    return size;
}

void MapGraphic::setCellSize(int newCellSize)
{
    if( newCellSize < 1 );
    else if( (newCellSize != m_cellSize) && (newCellSize <= 50) )
    {
       m_cellSize = newCellSize;
       update();
       updateGeometry();

    }
}

void MapGraphic::paintEvent(QPaintEvent *event)
{
    drawMap();
}

void MapGraphic::mousePressEvent(QMouseEvent *event)
{
    QPoint point = event->pos();

    if(event->buttons() & Qt::LeftButton)
    {
        if(m_map_type)//my board
        {
            if(!m_placement_over)
            {
                if(m_click_cnt != 2)
                {
                    int x = point.x()/m_cellSize;
                    int y = point.y()/m_cellSize;

                    m_ship_coords[m_click_cnt].setX(x);
                    m_ship_coords[m_click_cnt].setY(y);
                    m_click_cnt++;

                    if(m_click_cnt == 2)
                    {
                        m_click_cnt = 0;
                        emit sendCoords(m_ship_coords[0], m_ship_coords[1]);
                    }
                }

            }
        }
        else//enemy board
        {
            if(m_enable_enemy_map)
            {
                int x = point.x()/m_cellSize;
                int y = point.y()/m_cellSize;

                emit sendPosition(x, y);
            }
            else
            {
                QMessageBox::information(this, tr("Attention"), tr("Enemy doing step."),QMessageBox::Ok);
            }
        }
    }
}

void MapGraphic::clearMap()
{
    QPainter painter(this);
    painter.fillRect(m_rect, Qt::white);
    painter.setPen(m_curColor);

    for(int i = 0; i <= m_mapSize; ++i)
    {
        painter.drawLine(m_cellSize * i, 0,m_cellSize * i, m_cellSize * m_mapSize);
        painter.drawLine(0, m_cellSize * i, m_cellSize * m_mapSize, m_cellSize * i);
    }
}

void MapGraphic::drawShips()
{
    QPainter painter(this);
    painter.setPen(Qt::blue);

    QPoint point1, point2;
    Position start_pos, end_pos; //use to convert from my coords class to Qt coords class
    const Ship* ships_list = m_map->getShipsList();

    for(unsigned ship = 0; ship < m_map->getCurrentNumShips(); ship++)
    {
        start_pos = ships_list[ship].getStartPos();
        end_pos   = ships_list[ship].getEndPos();

        if(start_pos.getX() == end_pos.getX())
        {
                point1.setY(start_pos.getY()*m_cellSize);
                point1.setX(start_pos.getX()*m_cellSize);
                point2.setY((end_pos.getY()+1)*m_cellSize-1);
                point2.setX(end_pos.getX()*m_cellSize + m_cellSize-1);
        }
        else
        {
                point1.setX(start_pos.getX()*m_cellSize);
                point1.setY(start_pos.getY()*m_cellSize);
                point2.setX((end_pos.getX()+1)*m_cellSize-1);
                point2.setY(end_pos.getY()*m_cellSize + m_cellSize-1);
        }

        QRect rect;
        rect.setTopLeft(point1);
        rect.setBottomRight(point2);
        painter.drawRect(rect);
    }
}

void MapGraphic::drawAnotherPartsOnMap()
{
    QPainter painter(this);
    state_on_map** map = m_map->getMap();

    for( int y = 0; y < m_mapSize; y++ )
    {
        for( int x = 0; x < m_mapSize; x++ )
        {
            switch(map[y][x])
            {
                case hit_free_cell:
                    {
                        painter.setPen(Qt::blue);
                        int x_cord = x*m_cellSize + m_cellSize/2; //convert logic coords to widget coords
                        int y_cord = y*m_cellSize + m_cellSize/2;
                        painter.drawPoint(x_cord, y_cord);
                        break;
                    }

                case summed_ship:
                case destroyed_ship:
                    {
                        painter.setPen(Qt::blue);
                        QPoint point1, point2;

                        int x_cord = x*m_cellSize;
                        int y_cord = y*m_cellSize;

                        point1.setX(x_cord);
                        point1.setY(y_cord);

                        point2.setX(x_cord + m_cellSize-1);
                        point2.setY(y_cord + m_cellSize-1);

                        painter.drawLine(point1, point2);

                        //x stay old
                        point1.setY(y_cord + m_cellSize);
                        //y stay old
                        point2.setX(x_cord + m_cellSize-1);

                        painter.drawLine(point1, point2);

                        break;
                 }
            }
        }
    }
}

void MapGraphic::drawMap()
{
    clearMap();
    drawShips();
    drawAnotherPartsOnMap();
}
