#ifndef MAPGRAPHIC_H
#define MAPGRAPHIC_H

#include<QColor>
#include<QtGui>

#include"ships_map.h"

class MapGraphic: public QWidget
{
    Q_OBJECT
    Q_PROPERTY(QColor penColor READ penColor WRITE setPenColor)
    Q_PROPERTY(int    cellSize READ cellSize WRITE setCellSize)

public:
    MapGraphic(ShipsMap* map, bool map_type = true, QWidget* parent = 0, int cell_size = 30 );
    void setPenColor(const QColor& newColor) { m_curColor = newColor; }
    QColor penColor()const { return m_curColor; }
    void setCellSize(int newCellSize);
    int cellSize() const { return m_cellSize; }
    QSize sizeHint() const;
    void setPlasementFlag(bool value)
    {
        m_placement_over = value;
    }
    bool getPlasementFlag()
    {
        return m_placement_over;
    }
    void enableEnemyMap()
    {
        m_enable_enemy_map = true;
    }
    void disableEnemyMap()
    {
        m_enable_enemy_map = false;
    }

protected:
    void clearMap();
    void mousePressEvent(QMouseEvent* event);
    void paintEvent(QPaintEvent* event);


protected slots:
    void drawMap();
    void drawShips();
    void drawAnotherPartsOnMap();

signals:
    void sendCoords(QPoint start_pos, QPoint end_pos);
    void sendPosition(int x, int y);


protected:
    QRect  m_rect;
    QColor m_curColor;
    int    m_cellSize;


    ShipsMap* m_map;
    unsigned  m_mapSize;

    //mousePress events
    bool   m_map_type;    //true - my_board, false enemy_board
    unsigned m_click_cnt;
    bool     m_placement_over;//flag
    QPoint m_ship_coords[2];
    bool   m_enable_enemy_map;
};


#endif // MAPGRAPHIC_H
