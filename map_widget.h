#ifndef MAP_WIDGET_H
#define MAP_WIDGET_H

#include<QtGui>

#include"map_graphic.h"
#include"ships_map.h"

class MapWidget: public QWidget
{
    Q_OBJECT

public:
    MapWidget(bool map_type = true, unsigned map_size = 10, QWidget* parent = 0);
    QSize sizeHint() const;

protected slots:
    void recvCoords(QPoint start_pos, QPoint end_pos);
    void recvPosition(int x, int y);

protected:
    ShipsMap m_map;
    MapGraphic m_map_graphic;
    bool m_game_type; // true - Lan or false - AI
};

#endif // MAP_WIDGET_H
